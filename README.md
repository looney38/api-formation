# API

## Commandes
## Setup du projet

### Cloner le projet
- Avoir git d'installer
- Avoir une clé SSH
```
git clone git@gitlab.com:wehelp/api.git
```
### Avant de lancer le projet
```
```


## Git

#### branches principales
Deux branches fix:
* **master** est la branche où tout est stable. Chaque commit correspond à une version stable du projet (release) qui peut être déployée en production.

*  **develop** est la branche sur laquelle s’effectue le développement à proprement dit. On y prépare les changements en vue de la prochaine release dans master.
#### branches secondaires
les branches secondaires  se font et se défont avec le temps :
* **feature** part de **develop** et se merge dans **develop**.
* **release** part de **develop** et se merge dans **master** et **develop**.
* **hotfix** part de **master** et se merge dans **master** et **develop / release**.


  > [Aide git](https://www.nicoespeon.com/fr/2013/08/quel-git-workflow-pour-mon-projet/#le-git-flow)
#### Commandes
#####  branche feature
*Créer un feature*

> git checkout -b myFeature develop

*Finir une feature*
> git checkout develop    <br>
> git merge --no-ff myFeature <br>
> git branch -d myFeature <br>
> git push origin develop

#####  branche release
*Créer un release*

> git checkout -b release/vX.X develop <br>
>./bump-version.sh vX.X <br>
> git commit -a -m "Bumped version number to vX.X"

*Finir une release*
> **fusion de master et release** <br>
> git checkout master <br>
> git merge --no-ff release/vX.X <br>
> git tag -a vX.X <br>
>
>**fusion de develop et release** <br>
>git checkout develop <br>
>git merge --no-ff release/vX.X <br>
>
>**supression de la branche release** <br>
>git branch -d release/vX.X
>
#####  branche hotfix
*Créer un hotfix*

> git checkout -b hotfix/vX.X.X develop <br>
>./bump-version.sh vX.X.X <br>
> git commit -a -m "Bumped version number to vX.X.X"

*Finir une hotfix*
> **fusion de master et hotfix** <br>
> git checkout master <br>
> git merge --no-ff hotfix/vX.X.X <br>
> git tag -a vX.X.X <br>
>
>**fusion de develop et hotfix** <br>
>git checkout develop <br>
>git merge --no-ff hotfix/vX.X.X <br>
>
>**supression de la branche release** <br>
>git branch -d hotfix/vX.X.X


 #### convention de nommage

**Nommage des branches secondaires:**
* feature:  feature/nomBranche
* release:  release/nomBranche
* hotfix:  hotfix/nomBranche


**Nommage des commits**
WIP